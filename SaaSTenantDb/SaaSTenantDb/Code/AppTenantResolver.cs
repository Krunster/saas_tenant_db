﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using SaasKit.Multitenancy;
using SaaSTenantDb.Code.Models;
using SaaSTenantDb.Code.Repositories;

namespace SaaSTenantDb.Code
{
    public class AppTenantResolver : MemoryCacheTenantResolver<AppTenant>
    {
        private readonly SystemRepository _systemRepository;

        public AppTenantResolver(IMemoryCache cache, ILoggerFactory loggerFactory)
            :base(cache, loggerFactory)
        {
            _systemRepository = new SystemRepository();
        }

        protected override string GetContextIdentifier(HttpContext context)
        {
            return context.Request.Host.Value.ToLower();
        }

        protected override IEnumerable<string> GetTenantIdentifiers(TenantContext<AppTenant> context)
        {
            return new[] { context.Tenant.Hostname };
        }

        protected override Task<TenantContext<AppTenant>> ResolveAsync(HttpContext context)
        {
            TenantContext<AppTenant> tenantContext = null;
            var hostName = context.Request.Host.Value.ToLower();

            var tenant = _systemRepository.GetAppTenants().FirstOrDefault(
                t => t.Hostname.Equals(hostName));

            if (tenant != null)
            {
                tenantContext = new TenantContext<AppTenant>(tenant);
            }

            return Task.FromResult(tenantContext);
        }
    }
}
