﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using Npgsql;
using SaaSTenantDb.Code.Models;


namespace SaaSTenantDb.Code.Repositories
{
    public class SystemRepository
    {
        private readonly string connectionString = "User ID=asp_user;Password=Password123;Host=localhost;Port=5433;Database=TestDapper;Pooling=true;";

        public IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public IEnumerable<AppTenant> GetAppTenants()
        {
            using (IDbConnection conn = Connection)
            {

                string sQuery = "SELECT * FROM \"AppTenant\"";

                conn.Open();

                var result = conn.Query<AppTenant>(sQuery);

                return result;
            }
        }
    }
}
